DELIMITER $$

DROP PROCEDURE IF EXISTS getProductVolumeDiscounts;
CREATE PROCEDURE getProductVolumeDiscounts(v_customer_external_id VARCHAR(255), v_sku VARCHAR(255), v_pricelistid1 VARCHAR(5), v_pricelistid2 VARCHAR(5), v_pricelistid3 VARCHAR(5), v_itemgroupid VARCHAR(100))
BEGIN
    SET @vCustomerExternalId = v_customer_external_id;
    SET @vSku = v_sku;
    SET @vPriceListId1 = v_pricelistid1;
    SET @vPriceListId2 = v_pricelistid2;
    SET @vPriceListId3 = v_pricelistid3;
    SET @vItemGroupId = v_itemgroupid;
    SET @PriceFound = FALSE;
    SET @VALUE = 0;
    SET @PriceType = 0;


    SET @vDate = CURDATE();

    SET @Result = 0;


SELECT av_pricevolumes.value
FROM av_pricevolumes INNER JOIN av_specialprices ON
        av_pricevolumes.av_specialprices_id = av_specialprices.id
WHERE av_specialprices.customer_external_id = @vCustomerExternalId AND av_specialprices.sku = @vSku and (av_specialprices.date_from <= @vDate OR av_specialprices.date_from IS NULL) AND (av_specialprices.date_to >= @vDate OR av_specialprices.date_to IS NULL)
ORDER BY av_pricevolumes.quantity DESC, av_specialprices.discount_type ASC LIMIT 1 INTO @Result;

IF @Result > 0
    THEN
        SET @PriceFound = TRUE;
SELECT av_pricevolumes.quantity, av_pricevolumes.value, CAST(av_specialprices.discount_type AS UNSIGNED) AS discount_type
FROM av_pricevolumes INNER JOIN av_specialprices ON
        av_pricevolumes.av_specialprices_id = av_specialprices.id
WHERE av_specialprices.customer_external_id = @vCustomerExternalId AND av_specialprices.sku = @vSku and (av_specialprices.date_from <= @vDate OR av_specialprices.date_from IS NULL) AND (av_specialprices.date_to >= @vDate OR av_specialprices.date_to IS NULL)
ORDER BY av_pricevolumes.quantity DESC, av_specialprices.discount_type ASC;
END IF;

    IF(!@PriceFound AND @vSku <> '' AND (@vPriceListId1 <> '' OR @vPriceListId2 <> '' OR @vPriceListId3 <> ''))
    THEN
SELECT av_pricevolumes.value
FROM av_pricevolumes INNER JOIN av_specialprices ON
        av_pricevolumes.av_specialprices_id = av_specialprices.id
WHERE (av_specialprices.pricelist_id = @vPriceListId1 OR av_specialprices.pricelist_id = @vPriceListId2 OR av_specialprices.pricelist_id = @vPriceListId3) AND av_specialprices.sku = @vSku and (av_specialprices.date_from <= @vDate OR av_specialprices.date_from IS NULL) AND (av_specialprices.date_to >= @vDate OR av_specialprices.date_to IS NULL)
ORDER BY av_pricevolumes.quantity DESC, av_specialprices.discount_type ASC LIMIT 1 INTO @Result;


IF @Result > 0
        THEN
            SET @PriceFound = TRUE;
SELECT av_pricevolumes.quantity, av_pricevolumes.value, CAST(av_specialprices.discount_type AS UNSIGNED) AS discount_type
FROM av_pricevolumes INNER JOIN av_specialprices ON
        av_pricevolumes.av_specialprices_id = av_specialprices.id
WHERE (av_specialprices.pricelist_id = @vPriceListId1 OR av_specialprices.pricelist_id = @vPriceListId2 OR av_specialprices.pricelist_id = @vPriceListId3) AND av_specialprices.sku = @vSku and (av_specialprices.date_from <= @vDate OR av_specialprices.date_from IS NULL) AND (av_specialprices.date_to >= @vDate OR av_specialprices.date_to IS NULL)
ORDER BY av_pricevolumes.quantity DESC, av_specialprices.discount_type ASC;
END IF;
END IF;



    IF(!@PriceFound AND @vCustomerExternalId <> '' AND @vItemGroupId  <> '')
    THEN
SELECT av_pricevolumes.value
FROM av_pricevolumes INNER JOIN av_specialprices ON
        av_pricevolumes.av_specialprices_id = av_specialprices.id
WHERE av_specialprices.customer_external_id = @vCustomerExternalId AND av_specialprices.itemgroup_id = @vItemGroupId and (av_specialprices.date_from <= @vDate OR av_specialprices.date_from IS NULL) AND (av_specialprices.date_to >= @vDate OR av_specialprices.date_to IS NULL)
ORDER BY av_pricevolumes.quantity DESC, av_specialprices.discount_type ASC LIMIT 1 INTO @Result;

IF @Result > 0
        THEN
            SET @PriceFound = TRUE;
SELECT av_pricevolumes.quantity, av_pricevolumes.value, CAST(av_specialprices.discount_type AS UNSIGNED) AS discount_type
FROM av_pricevolumes INNER JOIN av_specialprices ON
        av_pricevolumes.av_specialprices_id = av_specialprices.id
WHERE av_specialprices.customer_external_id = @vCustomerExternalId AND av_specialprices.itemgroup_id = @vItemGroupId and (av_specialprices.date_from <= @vDate OR av_specialprices.date_from IS NULL) AND (av_specialprices.date_to >= @vDate OR av_specialprices.date_to IS NULL)
ORDER BY av_pricevolumes.quantity DESC, av_specialprices.discount_type ASC;
END IF;
END IF;


    IF(!@PriceFound AND (@vPriceListId1 <> '' OR @vPriceListId2 <> '' OR @vPriceListId3 <> '') AND @vItemGroupId <> '')
    THEN
SELECT av_pricevolumes.value
FROM av_pricevolumes INNER JOIN av_specialprices ON
        av_pricevolumes.av_specialprices_id = av_specialprices.id
WHERE (av_specialprices.pricelist_id = @vPriceListId1 OR av_specialprices.pricelist_id = @vPriceListId2 OR av_specialprices.pricelist_id = @vPriceListId3) AND av_specialprices.itemgroup_id = @vItemGroupId and (av_specialprices.date_from <= @vDate OR av_specialprices.date_from IS NULL) AND (av_specialprices.date_to >= @vDate OR av_specialprices.date_to IS NULL)
ORDER BY av_pricevolumes.quantity DESC, av_specialprices.discount_type ASC LIMIT 1 INTO @Result;

IF @Result > 0
        THEN
            SET @PriceFound = TRUE;
SELECT av_pricevolumes.quantity, av_pricevolumes.value, CAST(av_specialprices.discount_type AS UNSIGNED) AS discount_type
FROM av_pricevolumes INNER JOIN av_specialprices ON
        av_pricevolumes.av_specialprices_id = av_specialprices.id
WHERE (av_specialprices.pricelist_id = @vPriceListId1 OR av_specialprices.pricelist_id = @vPriceListId2 OR av_specialprices.pricelist_id = @vPriceListId3) AND av_specialprices.itemgroup_id = @vItemGroupId and (av_specialprices.date_from <= @vDate OR av_specialprices.date_from IS NULL) AND (av_specialprices.date_to >= @vDate OR av_specialprices.date_to IS NULL)
ORDER BY av_pricevolumes.quantity DESC, av_specialprices.discount_type ASC;
END IF;
END IF;
END $$
DELIMITER ;
