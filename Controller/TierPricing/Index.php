<?php

namespace Conneqt\AvSpecialPrices\Controller\TierPricing;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $jsonResultFactory;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;
    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $priceHelper;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollectionFactory;

    private $_productId;
    private $_product;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $session;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Registry $registry
    ) {
        parent::__construct($context);

        $this->productCollectionFactory = $productCollectionFactory;
        $this->resourceConnection = $resourceConnection;
        $this->customerRepository = $customerRepository;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->priceHelper = $priceHelper;
        $this->registry = $registry;
        $this->session = $session;
    }

    private function getProduct()
    {
        if (!$this->_product) {
            $productCollection = $this->productCollectionFactory->create()
                ->addIdFilter($this->_productId)
                ->addPriceData();
            $this->_product = $productCollection->getFirstItem();
        }
        return $this->_product;
    }

    public function getDiscountedPrice($discountPercentage)
    {
        $price = $this->getProduct()->getPrice() - ($this->getProduct()->getPrice() * ($discountPercentage / 100));
        return $this->priceHelper->currency(round($price, 2));
    }

    /**
     * Execute action based on request and return result
     *
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $this->_productId = $this->getRequest()->getParam('p');

        $response = $this->jsonResultFactory->create();
        $result = [];
        $response->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0', true);

        $customerExternalId = $this->getCustomerExternalId();
        if ($customerExternalId !== null) {
            $sku = $this->getProduct()->getSku();
            $pricelistIds = $this->getCustomerPricelistId() ?? '';
            $itemgroupId = $this->getProduct()->getItemgroupId();

            $connection = $this->resourceConnection->getConnection();

            $separator = ',';

            if(strpos($pricelistIds, ';')) {
                $separator = ';';
            }

            $pricelists = explode($separator, $pricelistIds);

            $pricelistId1 = $pricelists[0] ?? false;
            $pricelistId2 = $pricelists[1] ?? false;
            $pricelistId3 = $pricelists[2] ?? false;

            $result = $connection->fetchAll("CALL getProductVolumeDiscounts('{$customerExternalId}', '{$sku}', '{$pricelistId1}', '{$pricelistId2}', '{$pricelistId3}', '{$itemgroupId}');");

            if (count($result) > 0) {
                /* Sort result by quantity ascending */
                usort($result, function ($a, $b) {
                    if (intval($a['quantity']) == intval($b['quantity'])) {
                        return 0;
                    }
                    return (intval($a['quantity']) < intval($b['quantity'])) ? -1 : 1;
                });

                foreach ($result as $key => $tier) {
                    if ($tier['discount_type'] == '1') {
                        $result[$key]['price'] = $this->priceHelper->currency($tier['value']);
                    }

                    if ($tier['discount_type'] == '2') {
                        $result[$key]['price'] = $this->getDiscountedPrice($tier['value']);
                    }
                }
            }

            if (count($result) === 1 && $result[0]['quantity'] == 1) {
                $result = [];
            }
        }

        $response->setData($result);

        return $response;
    }

    private function getCustomerExternalId()
    {
        if ($this->session->getCustomerId()) {
            $customer = $this->customerRepository->getById($this->session->getCustomerId());

            return $customer->getCustomAttribute('external_id') ? $customer->getCustomAttribute('external_id')->getValue() : null;
        }

        return null;
    }

    private function getCustomerPricelistId()
    {
        if ($this->session->getCustomerId()) {
            $customer = $this->customerRepository->getById($this->session->getCustomerId());

            return $customer->getCustomAttribute('pricelist_id') ? $customer->getCustomAttribute('pricelist_id')->getValue() : null;
        }

        return null;
    }
}
