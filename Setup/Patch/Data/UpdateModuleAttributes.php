<?php
namespace Conneqt\AvSpecialPrices\Setup\Patch\Data;

class UpdateModuleAttributes implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Attribute
     */
    private $attributeResourceModel;
    /**
     * @var \Magento\Customer\Setup\CustomerSetupFactory
     */
    private $customerSetupFactory;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Model\Config$eavConfig,
        \Magento\Customer\Model\ResourceModel\Attribute $attributeResourceModel
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->eavConfig = $eavConfig;
        $this->attributeResourceModel = $attributeResourceModel;
    }

    public static function getDependencies()
    {
        return [
            AddCustomerAttributes::class,
            AddProductAttributes::class
        ];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $eavSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $attributesToUpdate = [
            'external_id',
            'pricelist_id',
        ];

        $entityType = \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER;
        $attributeSetId = $eavSetup->getDefaultAttributeSetId($entityType);
        $attributeGroupId = $eavSetup->getDefaultAttributeGroupId($entityType);

        foreach ($attributesToUpdate as $attributeCode) {
            /** @var \Magento\Eav\Model\Attribute $attribute */
            $attribute = $this->eavConfig->getAttribute($entityType, $attributeCode);

            $attribute->setData('attribute_set_id', $attributeSetId);
            $attribute->setData('attribute_group_id', $attributeGroupId);
            $attribute->setData('sort_order', 2000);

            $attribute->addData([
                'used_in_forms' => [
                    'adminhtml_customer'
                ]
            ]);

            $this->attributeResourceModel->save($attribute);
        }
    }
}
