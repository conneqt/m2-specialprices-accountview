<?php

namespace Conneqt\AvSpecialPrices\Setup\Patch\Data;

class AddCustomerAttributes implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /** @var \Magento\Framework\Setup\ModuleDataSetupInterface */
    protected $moduleDataSetup;

    /** @var \Magento\Customer\Setup\CustomerSetupFactory */
    protected $customerSetupFactory;
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;
    /**
     * @var \Magento\Customer\Model\ResourceModel\AttributeFactory
     */
    private $attributeResourceFactory;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\AttributeFactory $attributeResourceFactory,
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\Config $eavConfig
    ) {
        $this->attributeResourceFactory = $attributeResourceFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create();

        $customerSetup = $this->customerSetupFactory->create([
            'setup' => $this->moduleDataSetup
        ]);

        $customerEntity = $customerSetup->getEavConfig()->getEntityType(\Magento\Customer\Model\Customer::ENTITY);
        $attributeSetId = $customerSetup->getDefaultAttributeSetId($customerEntity->getEntityTypeId());
        $attributeGroup = $customerSetup->getDefaultAttributeGroupId($customerEntity->getEntityTypeId(), $attributeSetId);


        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'external_id',
            [
                'type' => 'varchar',
                'label' => 'External ID',
                'input' => 'text',
                'required' => false,
                'sort_order' => 1000,
                'visible' => true,
                'system' => false,
                'user_defined' => false
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'pricelist_id',
            [
                'type' => 'varchar',
                'label' => 'Pricelist ID',
                'input' => 'text',
                'required' => false,
                'sort_order' => 1000,
                'visible' => true,
                'system' => false,
                'user_defined' => false
            ]
        );

        $externalId = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'external_id');
        $externalId->addData([
            'used_in_forms' => ['adminhtml_customer'],
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroup
        ]);



        $pricelistId = $this->eavConfig->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'pricelist_id');
        $pricelistId->addData([
            'used_in_forms' => ['adminhtml_customer'],
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroup
        ]);


        $this->attributeResourceFactory->create()
            ->save($externalId)
            ->save($pricelistId);
    }
}
