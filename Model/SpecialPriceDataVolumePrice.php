<?php

namespace Conneqt\AvSpecialPrices\Model;

/**
 * @method \Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice getResource()
 * @method \Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice\Collection getCollection()
 */
class SpecialPriceDataVolumePrice extends \Magento\Framework\Model\AbstractModel implements \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'av_specialprices_specialpricedatavolumeprice';
    protected $_cacheTag = 'av_specialprices_specialpricedatavolumeprice';
    protected $_eventPrefix = 'av_specialprices_specialpricedatavolumeprice';

    protected function _construct()
    {
        $this->_init(\Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return mixed
     */
    public function getAvSpecialPriceId()
    {
        return $this->getData(self::FIELD_AV_SPECIALPRICE_ID);
    }

    /**
     * @var $specialPriceId int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setAvSpecialPriceId($specialPriceId)
    {
        $this->setData(self::FIELD_AV_SPECIALPRICE_ID, $specialPriceId);

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->getData(self::FIELD_QUANTITY);
    }

    /**
     * @param $quantity int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setQuantity($quantity)
    {
        $this->setData(self::FIELD_QUANTITY, $quantity);

        return $this;
    }

    /**
     * @return double
     */
    public function getValue()
    {
        return $this->getData(self::FIELD_VALUE);
    }

    /**
     * @param $value double
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setValue($value)
    {
        $this->setData(self::FIELD_VALUE, $value);

        return $this;
    }
}
