<?php

namespace Conneqt\AvSpecialPrices\Model\Calculator;

class SpecialPriceCalculator implements \Conneqt\SpecialPrices\Api\SpecialPriceCalculatorInterface
{
    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    /** @var \Magento\Catalog\Api\ProductRepositoryInterface */
    protected $productRepository;

    /** @var \Magento\Customer\Api\CustomerRepositoryInterface */
    protected $customerRepository;

    /** @var \Conneqt\AvSpecialPrices\Helper\StoredProcedure */
    protected $storedProcedureHelper;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Conneqt\AvSpecialPrices\Helper\StoredProcedure $storedProcedureHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->storedProcedureHelper = $storedProcedureHelper;
        $this->storeManager = $storeManager;
    }

    /**
     * Recalculates the price for a product
     *
     * @param $productId int
     * @param $customerId int
     * @param $basePrice
     * @param $qty int
     * @return double
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function calculate($productId, $customerId, $basePrice, $qty)
    {
        if ($basePrice == false) {
            $product = $this->productRepository->getById($productId);
            $basePrice = $product->hasData('special_price') ? $product->getData('special_price') : $product->getData('price');
        }

        $specialPricesEnabled = $this->scopeConfig->getValue('conneqt_specialprices/settings/active', \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT);

        if ($specialPricesEnabled != 1) {
            return $basePrice;
        }

        if (empty($qty)) {
            $qty = 1;
        }

        if ($customerId !== null) {
            $product = $this->productRepository->getById($productId);
            $customer = $this->customerRepository->getById($customerId);

            $priceListAttr = $customer->getCustomAttribute('pricelist_id');

            $priceListIds = $priceListAttr ? (string)$priceListAttr->getValue() : '';

            $separator = ',';

            if(strpos($priceListIds, ';')) {
                $separator = ';';
            }

            $pricelists = explode($separator, $priceListIds);


            $pricelistId1 = $pricelists[0] ?? false;
            $pricelistId2 = $pricelists[1] ?? false;
            $pricelistId3 = $pricelists[2] ?? false;

            $result = $this->storedProcedureHelper->callStoredProcedure(
                $this->storeManager->getWebsite()->getId(),
                $customer->getCustomAttribute('external_id') ? $customer->getCustomAttribute('external_id')->getValue() : null,
                $product->getSku(),
                $qty,
                date('d-m-y'),
                $this->storeManager->getStore()->getId(),
                $customer->getCustomAttribute('customergroup_id') ? $customer->getCustomAttribute('customergroup_id')->getValue() : null,
                $pricelistId1,
                $pricelistId2,
                $pricelistId3,
                $customer->getCustomAttribute('effective_discount') ? $customer->getCustomAttribute('effective_discount')->getValue() : null,
                $customer->getCustomAttribute('exclude_discount_groups') ? $customer->getCustomAttribute('exclude_discount_groups')->getValue() : null,
                $product->getCustomAttribute('itemgroup_id') ? $product->getCustomAttribute('itemgroup_id')->getValue() : null,
                $product->getCustomAttribute('manufacturer_id') ? $product->getCustomAttribute('manufacturer_id')->getValue() : null,
                $basePrice
            );

            if (!empty($result)) {
                if (!array_key_exists('PriceType', $result) || !array_key_exists('Value', $result)) {
                    return $basePrice;
                }

                if ($result['PriceType'] == null || $result['Value'] == null) {
                    return $basePrice;
                }

                $value = $result['Value'];
                $type = $result['PriceType'];

                if ($type == 1) {
                    return $value;
                }

                if ($type == 2) {
                    return $basePrice - (($basePrice / 100) * $value);
                }

                if ($type == 3) {
                    return $basePrice - $value;
                }
            }
        }

        return $basePrice;
    }
}
