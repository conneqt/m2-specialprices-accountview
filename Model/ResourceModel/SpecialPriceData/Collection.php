<?php

namespace Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Conneqt\AvSpecialPrices\Model\SpecialPriceData::class, \Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceData::class);
    }

    /**
     * @param $specialPriceId
     * @return \Conneqt\AvSpecialPrices\Model\SpecialPriceData
     */
    public function findBySpecialPriceId($specialPriceId)
    {
        return $this->getItemByColumnValue(\Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface::FIELD_SPECIALPRICE_ID, $specialPriceId);
    }
}
