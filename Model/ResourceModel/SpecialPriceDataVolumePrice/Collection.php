<?php

namespace Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Conneqt\AvSpecialPrices\Model\SpecialPriceDataVolumePrice::class, \Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice::class);
    }

    /**
     * @param $specialPriceId
     * @return \Conneqt\AvSpecialPrices\Model\SpecialPriceDataVolumePrice[]
     */
    public function findBySpecialPriceId($specialPriceId)
    {
        return $this->addFieldToFilter('av_specialprices_id', ['eq' => $specialPriceId])->getItems();
    }
}
