<?php

namespace Conneqt\AvSpecialPrices\Model\ResourceModel;

class SpecialPriceDataVolumePrice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('av_pricevolumes', 'id');
    }
}
