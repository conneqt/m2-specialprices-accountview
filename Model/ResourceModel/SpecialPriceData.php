<?php

namespace Conneqt\AvSpecialPrices\Model\ResourceModel;

class SpecialPriceData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('av_specialprices', 'id');
    }
}
