<?php

namespace Conneqt\AvSpecialPrices\Model;

/**
 * @method \Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceData getResource()
 * @method \Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceData\Collection getCollection()
 */
class SpecialPriceData extends \Magento\Framework\Model\AbstractModel implements \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'av_specialprices_specialpricedata';
    protected $_cacheTag = 'av_specialprices_specialpricedata';
    protected $_eventPrefix = 'av_specialprices_specialpricedata';

    /** @var \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface[] */
    protected $specialPriceVolumes = [];

    protected function _construct()
    {
        $this->_init(\Conneqt\AvSpecialPrices\Model\ResourceModel\SpecialPriceData::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return mixed
     */
    public function getSpecialPriceId()
    {
        return $this->getData(self::FIELD_SPECIALPRICE_ID);
    }

    /**
     * @var $specialPriceId int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setSpecialPriceId($specialPriceId)
    {
        $this->setData(self::FIELD_SPECIALPRICE_ID, $specialPriceId);

        return $this;
    }

    /**
     * @return string
     */
    public function getErpId()
    {
        return $this->getData(self::FIELD_ERP_ID);
    }

    /**
     * @param $erpId string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setErpId($erpId)
    {
        $this->setData(self::FIELD_ERP_ID, $erpId);

        return $this;
    }

    /**
     * @return int
     */
    public function getWebsiteId()
    {
        return $this->getData(self::FIELD_WEBSITE_ID);
    }

    /**
     * @param $websiteId int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setWebsiteId($websiteId)
    {
        $this->setData(self::FIELD_WEBSITE_ID, $websiteId);

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerExternalId()
    {
        return $this->getData(self::FIELD_CUSTOMER_EXTERNAL_ID);
    }

    /**
     * @param $customerExternalId string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setCustomerExternalId($customerExternalId)
    {
        $this->setData(self::FIELD_CUSTOMER_EXTERNAL_ID, $customerExternalId);

        return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->getData(self::FIELD_SKU);
    }

    /**
     * @param $sku string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setSku($sku)
    {
        $this->setData(self::FIELD_SKU, $sku);

        return $this;
    }

    /**
     * @return string
     */
    public function getItemgroupId()
    {
        return $this->getData(self::FIELD_ITEMGROUP_ID);
    }

    /**
     * @param $itemgroupId string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setItemgroupId($itemgroupId)
    {
        $this->setData(self::FIELD_ITEMGROUP_ID, $itemgroupId);

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomergroupId()
    {
        return $this->getData(self::FIELD_CUSTOMERGROUP_ID);
    }

    /**
     * @param $customergroupId string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setCustomergroupId($customergroupId)
    {
        $this->setData(self::FIELD_CUSTOMERGROUP_ID, $customergroupId);

        return $this;
    }

    /**
     * @return string
     */
    public function getPricelistId()
    {
        return $this->getData(self::FIELD_PRICELIST_ID);
    }

    /**
     * @param $pricelistId string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setPricelistId($pricelistId)
    {
        $this->setData(self::FIELD_PRICELIST_ID, $pricelistId);

        return $this;
    }

    /**
     * @return string
     */
    public function getManufacturerId()
    {
        return $this->getData(self::FIELD_MANUFACTURER_ID);
    }

    /**
     * @param $manufacturerId string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setManufacturerId($manufacturerId)
    {
        $this->setData(self::FIELD_MANUFACTURER_ID, $manufacturerId);

        return $this;
    }

    /**
     * @return string
     */
    public function getDiscountType()
    {
        return $this->getData(self::FIELD_DISCOUNT_TYPE);
    }

    /**
     * @param $discountType string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setDiscountType($discountType)
    {
        $this->setData(self::FIELD_DISCOUNT_TYPE, $discountType);

        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom()
    {
        return $this->getData(self::FIELD_DATE_FROM);
    }

    /**
     * @param $dateFrom string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setDateFrom($dateFrom)
    {
        $this->setData(self::FIELD_DATE_FROM, $dateFrom);

        return $this;
    }

    /**
     * @return string
     */
    public function getDateTo()
    {
        return $this->getData(self::FIELD_DATE_TO);
    }

    /**
     * @param $dateTo string
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setDateTo($dateTo)
    {
        $this->setData(self::FIELD_DATE_TO, $dateTo);

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->getData(self::FIELD_QUANTITY);
    }

    /**
     * @param $quantity int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setQuantity($quantity)
    {
        $this->setData(self::FIELD_QUANTITY, $quantity);

        return $this;
    }

    /**
     * @return double
     */
    public function getValue()
    {
        return $this->getData(self::FIELD_VALUE);
    }

    /**
     * @param $value double
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setValue($value)
    {
        $this->setData(self::FIELD_VALUE, $value);

        return $this;
    }

    /**
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface[]
     */
    public function getPriceVolumes()
    {
        return $this->specialPriceVolumes;
    }

    /**
     * @param \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface[] $priceVolumes
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setPriceVolumes($priceVolumes)
    {
        $this->specialPriceVolumes = $priceVolumes;
    }

    /**
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setUpdatedAt(){
        $this->setData(self::FIELD_UPDATED_AT, new \DateTime());

        return $this;
    }
}
