<?php

namespace Conneqt\AvSpecialPrices\Api\Data;

interface SpecialPriceDataVolumePriceInterface
{
    const FIELD_ID = 'id';
    const FIELD_AV_SPECIALPRICE_ID = 'av_specialprices_id';
    const FIELD_QUANTITY = 'quantity';
    const FIELD_VALUE = 'value';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getAvSpecialPriceId();

    /**
     * @var $specialPriceId int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setAvSpecialPriceId($specialPriceId);

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * @param $quantity int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setQuantity($quantity);

    /**
     * @return double
     */
    public function getValue();

    /**
     * @param $value double
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setValue($value);
}
