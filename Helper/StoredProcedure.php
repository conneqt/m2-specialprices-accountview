<?php

namespace Conneqt\AvSpecialPrices\Helper;

class StoredProcedure
{
    const STORED_PROCEDURE_PREFIX = 'av_specialprice_calculation_';

    /** @var \Magento\Framework\App\ResourceConnection */
    protected $resourceConnection;

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->scopeConfig = $scopeConfig;
    }

    public function getStoredProcedureName($websiteId)
    {
        $storedProcedureDefault = $this->scopeConfig->getValue(\Conneqt\AvSpecialPrices\Observer\CreateStoredProcedure::CONFIG_PATH_STORED_PROCEDURE, \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        $storedProcedureWebsite = $this->scopeConfig->getValue(\Conneqt\AvSpecialPrices\Observer\CreateStoredProcedure::CONFIG_PATH_STORED_PROCEDURE, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE, $websiteId);

        if ($storedProcedureDefault == $storedProcedureWebsite) {
            $websiteId = 0;
        }

        return self::STORED_PROCEDURE_PREFIX . $websiteId;
    }

    public function createStoredProcedure($name, $innerProcedure)
    {
        $connection = $this->resourceConnection->getConnection();

        $deleteProcedure = 'DROP PROCEDURE IF EXISTS ' . $name;
        $createProcedure = <<<____SQLPRC
CREATE PROCEDURE {$name}(v_website_id INT, v_customer_external_id VARCHAR(255), v_sku VARCHAR(255), v_qty INT, v_date VARCHAR(50), v_storeid INT, v_customergroupid VARCHAR(100), v_pricelistid1 VARCHAR(5), v_pricelistid2 VARCHAR(5), v_pricelistid3 VARCHAR(5), v_effectivediscount VARCHAR(100), v_excludediscountgroups INT, v_itemgroupid VARCHAR(100), v_manufacturerid VARCHAR(100), v_standardprice DECIMAL(10,6))
BEGIN
	SET @vWebsiteId = v_website_id;
	SET @vCustomerExternalId = v_customer_external_id;
	SET @vSku = v_sku;
	SET @vQuantity = v_qty;
	SET @vDate = v_date;
	SET @vStoreId = v_storeid;
	SET @vCustomerGroupId = v_customergroupid;
	SET @vPriceListId1 = v_pricelistid1;
	SET @vPriceListId2 = v_pricelistid2;
	SET @vPriceListId3 = v_pricelistid3;
	SET @vEffectiveDiscount = v_effectivediscount;
	SET @vExcludeDiscountGroups = v_excludediscountgroups;
	SET @vItemGroupId = v_itemgroupid;
	SET @vManufacturerId = v_manufacturerid;
	SET @vStandardPrice = v_standardprice;

	@@PROCEDURE@@
END
____SQLPRC;

        $createProcedure = str_replace('@@PROCEDURE@@', $innerProcedure, $createProcedure);
        $connection->multiQuery($deleteProcedure);
        $connection->multiQuery($createProcedure);
    }

    public function callStoredProcedure(
        $websiteId,
        $customerExternalId,
        $sku,
        $qty,
        $date,
        $storeId,
        $customerGroupId,
        $pricelistId1,
        $pricelistId2,
        $pricelistId3,
        $effectiveDiscount,
        $excludeDiscountGroups,
        $itemGroupId,
        $manufacturerId,
        $standardPrice
    ) {
        try {
            $connection = $this->resourceConnection->getConnection();

            $result = $connection->fetchRow("CALL {$this->getStoredProcedureName($websiteId)}({$websiteId}, '{$customerExternalId}', '{$sku}', {$qty}, '{$date}', '{$storeId}', '{$customerGroupId}', '{$pricelistId1}', '{$pricelistId2}', '{$pricelistId3}', '{$effectiveDiscount}', '{$excludeDiscountGroups}', '{$itemGroupId}', '{$manufacturerId}', '{$standardPrice}')");

            return $result;
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());
        }
    }
}
